using namespace std;

#include <vector>
#include <map>
#include <iostream>
#include <limits>

void setup();

int power_2(int x)
{
    return x * x;
}

// no need to take the square root as dealing with integer is easier
int calculate_distance(pair<int, int> s1, pair<int, int> s2)
{
    return power_2(s1.first - s2.first) + power_2(s1.second - s2.second);
}

int main()

{
    setup(); // to read from file

    int N, M, k, x, y;
    int min_distance, distance;
    string s, min_s;
    map<string, string> best_station;
    map<string, pair<int, int>> swvl_stations, suggested_stations;
    vector<string> suggested_stations_ordered;

    cin >> N;
    while (N--)
    {
        cin >> k;
        while (k--)
        {

            cin >> s >> x >> y;
            swvl_stations[s] = make_pair(x, y);
        }
    }
    cin >> M;
    while (M--)
    {
        cin >> s >> x >> y;

        suggested_stations[s] = make_pair(x, y);
        suggested_stations_ordered.push_back(s); // helps in priniting the ouput in correct order
    }

    for (auto const &s : suggested_stations)
    {
        min_distance = numeric_limits<int>::max();
        for (auto const &swvl_s : swvl_stations)
        {
            distance = calculate_distance(s.second, swvl_s.second);
            if (distance < min_distance)
            {
                min_distance = distance;
                min_s = swvl_s.first;
            }
            best_station[s.first] = min_s;
        }
    }

    for (auto const &ss : suggested_stations_ordered)
    {
        cout << ss << " " << best_station[ss] << endl;
    }

    return 0;
}

void setup()
{
    ios_base::sync_with_stdio(false);
#ifndef ONLINE_JUDGE
    // freopen("input.in", "rt", stdin); // uncomment this to read from file
    //freopen("task1-test-output.txt", "wt", stdout);
#endif // ONLINE_JUDGE
}
